@ECHO OFF
@pyinstaller -w -F ./src/main/projects-manager.py
@copy dist\projects-manager.exe .
@rd /S /Q build\
@rd /S /Q dist\
@pause
