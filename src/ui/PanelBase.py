#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Build-in / Std

'''
Created on 2016年8月25日

@author: Hawk
'''
import wx
from wx.lib.pubsub import pub as Publisher
from misc.Enum import Enum

ControlID = Enum([
'MENUBAR_MENU_ITEM_REFRESH = 1000',
'MENUBAR_MENU_ITEM_CREATE',
'MENUBAR_MENU_ITEM_SHOW_STATUSBAR',
'MENUBAR_MENU_ITEM_HIDE_STATUSBAR',
'MENUBAR_MENU_ITEM_EXPORT_ALL',
'MENUBAR_MENU_ITEM_EXPORT_ALL_CHM_UTF8',
'MENUBAR_MENU_ITEM_EXPORT_ALL_CHM_GBK',
'MENUBAR_MENU_ITEM_EXPORT_ALL_HTML',
])

#Publisher.sendMessage('object.added')
class PanelBase(wx.Panel):

    def __init__(self, *args, **kwargs):
        wx.Panel.__init__(self, *args, **kwargs)
        #self.button = wx.Button(self, ControlID.MENUBAR_MENU_ITEM_CREATE, "Hello", pos=(50, 20))
        #self.button1 = wx.Button(self, -1, "Hello", pos=(50, 20))
        #self.BindButton(self.button, ControlID.MENUBAR_MENU_ITEM_CREATE, self.OnClick, kw={'11':'222'})
        #self.button.SetDefault()
        #self.SetBackgroundColour(wx.Colour(0, 0, 240))

        #self.main_sizer = wx.BoxSizer(wx.HORIZONTAL)
        #self.main_sizer.AddStretchSpacer() #Add((0, 0), 1, wx.EXPAND)
        #self.main_sizer.Add(self.button, 0, wx.CENTER)
        #self.main_sizer.Add(self.button1, 0, wx.CENTER)
        #self.main_sizer.AddStretchSpacer() #Add((0, 0), 1, wx.EXPAND)
        #self.SetSizer(self.main_sizer)

    def showMessageBox(self, text, caption=u"提示", style=wx.OK):
        dlg = wx.MessageDialog(None, text, caption, style)
        if dlg.ShowModal() == wx.ID_YES:
            dlg.Close(True)
        dlg.Destroy()

    def OnClick(self, event, id, **kwargs):
        print id.name
        print event.GetId()
        print kwargs
        print self
        Publisher.sendMessage("click_on_panel", data=True, extra1=0)
        self.showMessageBox('Hi')

    def BindEx(self, ctrl, ctrl_id, event_id, callback, **kwargs):
        ctrl.SetId(ctrl_id)
        self.Bind(
                  event_id,
                  lambda event, id=ctrl_id, a_kwargs=kwargs : callback(event, id, **a_kwargs),
                  ctrl)
    def BindButton(self, ctrl, ctrl_id, callback, **kwargs):
        return self.BindEx(ctrl, ctrl_id, wx.EVT_BUTTON, callback, **kwargs)
    def BindMenu(self, ctrl, ctrl_id, callback, **kwargs):
        return self.BindEx(ctrl, ctrl_id, wx.EVT_MENU, callback, **kwargs)
