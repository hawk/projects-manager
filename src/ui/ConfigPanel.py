#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Build-in / Std
from wx.build.build_options import WX_CONFIG

'''
Created on 2016年8月24日

@author: Hawk
'''
import wx
import images_icon
from PanelBase import PanelBase
from SkuMgrPanel import SkuMgrPanel
from ProjectMgrPanel import ProjectMgrPanel
from ProjectSkuMgrPanel import ProjectSkuMgrPanel
from CategoryMgrPanel import CategoryMgrPanel
from wx.lib.pubsub import pub as Publisher

class ConfigPanel(PanelBase):
    def __init__(self, *args, **kwargs):
        PanelBase.__init__(self, *args, **kwargs)
        self.SetBackgroundColour(wx.Colour(0, 0, 240))

        self.sku = SkuMgrPanel(self, -1)
        self.project = ProjectMgrPanel(self, -1)
        self.category = CategoryMgrPanel(self, -1)
        self.projectsku = ProjectSkuMgrPanel(self, -1)

        self.sizer = wx.GridBagSizer(hgap=1, vgap=1)
        self.sizer.Add(self.sku, pos=(0, 0), span=(1, 1), flag=wx.EXPAND | wx.ALL)
        self.sizer.Add(self.project, pos=(0, 1), span=(1, 1), flag=wx.EXPAND | wx.ALL)
        self.sizer.Add(self.category, pos=(1, 0), span=(1, 1), flag=wx.EXPAND | wx.ALL)
        self.sizer.Add(self.projectsku, pos=(1, 1), span=(1, 1), flag=wx.EXPAND | wx.ALL)

        #self.main_sizer = wx.BoxSizer(wx.VERTICAL)

        #self.top_sizer = wx.BoxSizer(wx.HORIZONTAL)
        #self.bottom_sizer = wx.BoxSizer(wx.HORIZONTAL)

        #self.main_sizer.Add(self.top_sizer, proportion=1, flag=wx.EXPAND, border=1)
        #self.main_sizer.Add(self.bottom_sizer, proportion=1, flag=wx.EXPAND, border=1)

        #self.top_sizer.Add(self.sku, proportion=1, flag=wx.EXPAND, border=1)
        #self.top_sizer.Add(self.project, proportion=1, flag=wx.EXPAND, border=1)

        #self.bottom_sizer.Add(self.category, proportion=1, flag=wx.EXPAND, border=1)
        #self.bottom_sizer.Add(self.projectsku, proportion=1, flag=wx.EXPAND, border=1)

        #self.panel = PanelBase.PanelBase(self, -1, size=(100, 100))
        #self.panel1 = SkuMgrPanel.SkuMgrPanel(self, -1, size=(200, 200))
        #self.panel2 = PanelBase.PanelBase(self, -1)

        #self.sizer_left = wx.BoxSizer(wx.VERTICAL)
        #self.sizer_left.Add(
        #    self.panel1,
        #    proportion=1,
        #    flag=wx.ALL | wx.EXPAND,
        #    border=1)
        #self.sizer_left.Add(
        #    self.panel2,
        #    proportion=1,
        #    flag=wx.ALL | wx.EXPAND,
        #    border=1)

        #self.baseBoxSizer = wx.BoxSizer(wx.HORIZONTAL)
        #self.baseBoxSizer.Add(
        #    self.sizer_left,
        #    proportion=1,
        #    flag=wx.ALL | wx.EXPAND,
        #    border=1)
        #self.baseBoxSizer.Add(
        #    self.panel1,
        #    proportion=1,
        #    flag=wx.ALL | wx.EXPAND,
        #    border=1)
        self.SetSizer(self.sizer)
        #self.SetSizerAndFit(self.sizer)
        self.sizer.Fit(self)
        self.Layout()
        self.Center()

        Publisher.subscribe(self.__onObjectAdded, 'click_on_panel')

    def __onObjectAdded(self, data, extra1, extra2=None):
        # no longer need to access data through message.data.
        print 'Object', repr(data), 'is added'
        print extra1
        print extra2
        self.Layout()
