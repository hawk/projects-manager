#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Build-in / Std

'''
Created on 2016年9月1日

@author: Hawk
'''
import wx
from IBaseFunction import IBaseFunction

class MDIChildFrameBase(wx.MDIChildFrame, IBaseFunction):
    def __init__(self, *args, **kwargs):
        wx.MDIChildFrame.__init__(self, *args, **kwargs)
        self.Bind(wx.EVT_ACTIVATE, self.OnActiveEvent)
        self.Bind(wx.EVT_CLOSE, self.OnClose)

    def OnClose(self, event):
        self.Iconize()

    def OnActiveEvent(self, event):
        if event.GetActive():
            self.OnActive()
        else:
            self.OnDeactive()

    def OnActive(self):
        pass
    def OnDeactive(self):
        pass
    def showMessageBox(self, text, caption=u"提示", style=wx.OK):
        dlg = wx.MessageDialog(None, text, caption, style)
        if dlg.ShowModal() == wx.ID_YES:
            dlg.Close(True)
        dlg.Destroy()

    def BindEx(self, ctrl, ctrl_id, event_id, callback, **kwargs):
        ctrl.SetId(ctrl_id)
        self.Bind(
                  event_id,
                  lambda event, id=ctrl_id, a_kwargs=kwargs : callback(event, id, **a_kwargs),
                  ctrl)
    def BindButton(self, ctrl, ctrl_id, callback, **kwargs):
        return self.BindEx(ctrl, ctrl_id, wx.EVT_BUTTON, callback, **kwargs)
    def BindMenu(self, ctrl, ctrl_id, callback, **kwargs):
        return self.BindEx(ctrl, ctrl_id, wx.EVT_MENU, callback, **kwargs)
