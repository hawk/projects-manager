#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Build-in / Std

'''
Created on 2016年9月1日

@author: Hawk
'''
import sys
import wx
import wx.grid
import MDIChildFrameBase
import BaseDialog
from model.Model import *
from action.Action import *
from misc.Excel import *
from ControlID import ControlID
import datetime
import calendar

class SummaryByProductFrame(MDIChildFrameBase.MDIChildFrameBase):
    menu_title_in_menubar = "&Summary"

    def __init__(self, *args, **kwargs):
        MDIChildFrameBase.MDIChildFrameBase.__init__(self, *args, **kwargs)
        self.InitMenu()

        self.panel = wx.Panel(self)
        self.summary_grid = wx.grid.Grid(
            self.panel,
            - 1,
            style=wx.LC_REPORT)

        self.summary_grid.CreateGrid(1, 1)

        #wx.EVT_LIST_ITEM_RIGHT_CLICK(
        #    self.ListCtrl_ProjectList,
        #    - 1,
        #    self.OnProjectListRightClick)

        self.last_hover_pos = (0, 0)

        self.merged_cells = []

        self.InitData()

        self.sizer_left = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer_left.Add(self.summary_grid, 1, wx.EXPAND | wx.ALL)
        self.panel.SetSizer(self.sizer_left)
        self.sizer_left.Fit(self)

    def ResizeGrid(self, new_row_count, new_col_count):
        self.summary_grid.DeleteRows(0, self.summary_grid.GetNumberRows())
        self.summary_grid.DeleteCols(0, self.summary_grid.GetNumberCols())

        self.summary_grid.AppendRows(new_row_count)
        self.summary_grid.AppendCols(new_col_count)

    def InitData(self):
        #if hasattr(self, 'summary_grid') and self.summary_grid is not None:
        #    self.summary_grid.Destroy()
        self.product_list = []#Product.select().where(Product.deleted == False).group_by()
        for cat in Category.select().where(Category.deleted == False):
            for project in cat.projects:
                self.product_list.extend(project.products)

        self.table_header_fixed_row_count = 2
        self.table_header_first_row_idx = 0
        self.table_header_second_row_idx = 1

        # Fill header
        self.table_headers = [u'Category', u'Project', u'Sku', u'Form107 Date', u'MP date']
        self.year_labels = [ u'%d' % y for y in range(2009, datetime.datetime.now().year + 2)]#[u'2012', u'2013', u'2014', u'2015', u'2016']
        self.month_labels = [u'1', u'2', u'3', u'4', u'5', u'6', u'7', u'8', u'9', u'10', u'11', u'12']

        self.row_count = len(self.product_list) + self.table_header_fixed_row_count
        self.col_count = len(self.table_headers) + 12 * len(self.year_labels)

        self.ResizeGrid(self.row_count, self.col_count)

        for col_idx, text in enumerate(self.table_headers):
            self.summary_grid.SetCellSize(self.table_header_first_row_idx, col_idx, 2, 1);
            self.summary_grid.SetCellAlignment(self.table_header_first_row_idx, col_idx, wx.ALIGN_CENTRE, wx.ALIGN_CENTRE);
            self.summary_grid.SetCellValue(self.table_header_first_row_idx, col_idx, text)

        for idx, text in enumerate(self.year_labels):
            col_idx = idx * 12 + len(self.table_headers)
            self.summary_grid.SetCellSize(self.table_header_first_row_idx, col_idx, 1, 12);
            self.summary_grid.SetCellAlignment(self.table_header_first_row_idx, col_idx, wx.ALIGN_CENTRE, wx.ALIGN_CENTRE);
            self.summary_grid.SetCellValue(self.table_header_first_row_idx, col_idx, text)

            for month_idx, month_text in enumerate(self.month_labels):
                self.summary_grid.SetCellAlignment(self.table_header_second_row_idx, col_idx + month_idx, wx.ALIGN_CENTRE, wx.ALIGN_CENTRE);
                self.summary_grid.SetCellValue(self.table_header_second_row_idx, col_idx + month_idx, month_text)
                self.summary_grid.AutoSizeColumn(col_idx + month_idx)

        # Fill data
        for idx, product in enumerate(self.product_list):
            row_idx = idx + self.table_header_fixed_row_count
            self.summary_grid.SetCellValue(row_idx, 0, product.project.category.name)
            self.summary_grid.SetCellValue(row_idx, 1, product.project.name)
            self.summary_grid.SetCellValue(row_idx, 2, product.sku.name)
            self.summary_grid.SetCellValue(row_idx, 3, str(product.frm107_date))
            self.summary_grid.SetCellValue(row_idx, 4, str(product.mp_date))

            # Get release date
            records = Record.select().where((Record.product == product.id) & (Record.deleted == False))
            for record in records:
                col_idx = self.getColumIndexByDate(record.date)
                value = self.summary_grid.GetCellValue(row_idx, col_idx).strip()
                if record.is_mp:
                    pass
                elif cmp(value, '') == 0:
                    self.summary_grid.SetCellValue(row_idx, col_idx, '1')
                else:
                    self.summary_grid.SetCellValue(row_idx, col_idx, str(int(value) + 1))

                if record.is_mp:
                    self.summary_grid.SetCellBackgroundColour(row_idx, col_idx, "red")
                elif record.is_sync_ww:
                    self.summary_grid.SetCellBackgroundColour(row_idx, col_idx, "green")
                else:
                    self.summary_grid.SetCellBackgroundColour(row_idx, col_idx, "yellow")

                self.summary_grid.SetCellAlignment(row_idx, col_idx, wx.ALIGN_CENTRE, wx.ALIGN_CENTRE);

            self.summary_grid.GetGridWindow().Bind(wx.EVT_MOTION, self.onMouseOver)

    def onMouseOver(self, event):
        """
        Displays a tooltip over any cell in a certain column
        """
        # Use CalcUnscrolledPosition() to get the mouse position within the 
        # entire grid including what's offscreen
        # This method was suggested by none other than Robin Dunn
        x, y = self.summary_grid.CalcUnscrolledPosition(event.GetX(), event.GetY())
        coords = self.summary_grid.XYToCell(x, y)
        col = coords[1]
        row = coords[0]

        if self.last_hover_pos == (row, col):
            return

        self.last_hover_pos = (row, col)
        records = self.getRecordsAt(row, col)
        #print '(%d, %d)' % (row, col), "record count:%d" % len(records)

        if len(records) > 0:
            msg = ''
            for r in records:
                if r.is_mp:
                    msg += "{date} {version} {wwversion} {mm} *\n".format(date=r.date, version=r.version, wwversion=r.ww_version, mm=r.development_mm)
                else:
                    msg += "{date} {version} {wwversion} {mm}\n".format(date=r.date, version=r.version, wwversion=r.ww_version, mm=r.maintenance_mm)
            event.GetEventObject().SetToolTipString(msg)
        else:
            event.GetEventObject().UnsetToolTip()

    def getRecordsAt(self, row_idx, col_idx):
        product_idx = row_idx - self.table_header_fixed_row_count
        month_idx = col_idx - len(self.table_headers)

        if product_idx >= 0 and product_idx < len(self.product_list) and month_idx > 0 and month_idx < len(self.year_labels) * 12:
            year_idx = month_idx / 12
            month_idx = month_idx % 12

            product = self.product_list[product_idx]

            year = self.year_labels[year_idx]
            month = self.month_labels[month_idx]

            start_date_value = datetime.date(int(year), int(month), 1) #int("%d%02d%02d" % (int(year), int(month), 0))
            end_date_value = datetime.date(int(year), int(month), calendar.monthrange(int(year), int(month))[1]) #int("%d%02d%02d" % (int(year), int(month), 32))

            return Record.select().where((Record.product == product.id) & (Record.date >= start_date_value) & (Record.date <= end_date_value))

        return []

    def getColumIndexByDate(self, release_date):
        fixed_col_count = len(self.table_headers)
        index_of_year = self.year_labels.index(str(release_date.year))

        index_of_month = self.month_labels.index(str(release_date.month))
        col_idx = fixed_col_count + index_of_year * 12 + index_of_month

        return col_idx

    def InitMenu(self):
        self.menuBar_menubar_menu_items = [
            {'id': ControlID.MENUBAR_MENU_ITEM_SKU_ADD, 'title': u'Add', 'separator': False, 'IsShown': lambda : False},
            {'id': ControlID.MENUBAR_MENU_ITEM_SKU_REFRESH, 'title': u'Refresh', 'separator': False, 'IsShown': lambda : True,
                'hasSub': False,
                'sub' : [{'id': ControlID.MENUBAR_MENU_ITEM_CATEGORY_ADD, 'title': u"导出为CHM(UTF-8)"}]},
            {'id': ControlID.MENUBAR_MENU_ITEM_EXPORT_XLS, 'title': u'Export', 'separator': False, 'IsShown': lambda : True},
            ]
        self.menu = self.BuildMenuItems(self.menuBar_menubar_menu_items, self.OnMenubarMenu_Select)

    def OnMenubarMenu_Select(self, event, menu_item, path):
        id = event.GetId()
        if id == ControlID.MENUBAR_MENU_ITEM_SKU_ADD:
            pass
        elif id == ControlID.MENUBAR_MENU_ITEM_SKU_REFRESH:
            self.InitData()
        elif id == ControlID.MENUBAR_MENU_ITEM_EXPORT_XLS:
            self.ExportXLSX()

    def _gen_cell_range(self, start_row_idx, start_col_idx, end_row_idx, end_col_idx):
        cells = []
        for row in range(start_row_idx, end_row_idx):
            for col in range(start_col_idx, end_col_idx):
                cells.append((row, col))

        return cells

    def AddMergedCells(self, start_row_idx, start_col_idx, end_row_idx, end_col_idx):
        if not hasattr(self, 'merged_cells') or self.merged_cells is None:
            self.merged_cells = []
        self.merged_cells.extend(self._gen_cell_range(start_row_idx, start_col_idx, end_row_idx, end_col_idx))

    def ExportXLSX(self):
        row_count = self.summary_grid.GetNumberRows()
        col_count = self.summary_grid.GetNumberCols()

        data_range_cells = self._gen_cell_range(
                                                0,
                                                len(self.table_headers),
                                                row_count,
                                                col_count)

        import xlsxwriter
        import misc.XLSX as xlsx

        saved_file_path = ""

        dialog = wx.FileDialog(None, xlsx.excel_file_chose_caption, os.getcwd(),
                xlsx.gen_export_xlsx_file_name("SummaryByProduct"), xlsx.excel_file_chose_wildcard, wx.SAVE)
        if dialog.ShowModal() == wx.ID_OK:
            saved_file_path = dialog.GetPath()
        else:
            return

        if not saved_file_path.endswith(".xlsx"):
            self.showMessageBox(u"导出失败! 文件扩展名必须是xlsx!")
            return

        workbook = xlsxwriter.Workbook(saved_file_path, {'default_date_format': 'dd/mm/yy'})
        worksheet = workbook.add_worksheet()

        self.merged_cells = []
        for row_idx in range(row_count):
            for col_idx in range(col_count):
                row_h, col_w = self.summary_grid.GetCellSize(row_idx, col_idx)
                value = self.summary_grid.GetCellValue(row_idx, col_idx).strip()
                cell_type = self.GetCellType(row_idx, col_idx)

                if cell_type == 3 or cmp(value, '') != 0:
                    if (row_idx, col_idx) not in self.merged_cells:
                        worksheet.set_row(row_idx, 15)
                        worksheet.set_column(col_idx, col_idx, 15)
                        if (row_idx, col_idx) in data_range_cells:
                            if cell_type == 1:
                                xlsx.write_cell(workbook, worksheet, row_idx, col_idx, int(value), row_height=row_h, col_width=col_w, format=xlsx.green_bkg_format)
                            elif cell_type == 2:
                                xlsx.write_cell(workbook, worksheet, row_idx, col_idx, int(value), row_height=row_h, col_width=col_w, format=xlsx.yellow_bkg_format)
                            elif cell_type == 3:
                                xlsx.write_cell(workbook, worksheet, row_idx, col_idx, value, row_height=row_h, col_width=col_w, format=xlsx.red_bkg_format)
                            else:
                                xlsx.write_cell(workbook, worksheet, row_idx, col_idx, int(value), row_height=row_h, col_width=col_w)

                            records = self.getRecordsAt(row_idx, col_idx)
                            if len(records) > 0:
                                comment = 'Best:\n'
                                for r in records:
                                    if r.is_mp:
                                        comment += "{date} {version} {wwversion} {mm} *\n".format(date=r.date, version=r.version, wwversion=r.ww_version, mm=r.development_mm)
                                    else:
                                        comment += "{date} {version} {wwversion} {mm}\n".format(date=r.date, version=r.version, wwversion=r.ww_version, mm=r.maintenance_mm)
                                worksheet.write_comment(row_idx, col_idx, comment, {'width': 140, 'height': (len(records) + 1) * 20, 'author': 'Best'})

                            worksheet.set_column(col_idx, col_idx, 2)
                            #[u'Category', u'Project', u'Sku', u'Form107 Date', u'MP date']
                        elif col_idx in (self.table_headers.index('Form107 Date'), self.table_headers.index('MP date')) and row_idx > 1:
                            xlsx.write_cell(workbook, worksheet, row_idx, col_idx, datetime.datetime.strptime(value, "%Y-%m-%d").date(), row_height=row_h, col_width=col_w, format=xlsx.date_format)
                            worksheet.set_column(col_idx, col_idx, 12)
                        elif col_idx in (self.table_headers.index('Project'), self.table_headers.index('Sku'), self.table_headers.index('Category')):
                            xlsx.write_cell(workbook, worksheet, row_idx, col_idx, value, row_height=row_h, col_width=col_w, format=xlsx.default_align_left_format)
                        else:
                            xlsx.write_cell(workbook, worksheet, row_idx, col_idx, value, row_height=row_h, col_width=col_w)
                        if row_h > 1 or col_w > 1:
                            self.AddMergedCells(row_idx, col_idx, row_idx + row_h, col_idx + col_w)

        category_group_idx_list = xlsx.group_and_outer_border(workbook, worksheet, 0, self.summary_grid, 2)
        #gl = group_and_outer_border(1, self.summary_grid, 2)

        project_group_idx_list = xlsx.group_col_data(self.table_headers.index('Project'), self.summary_grid, 2)

        xlsx.apply_group(workbook, worksheet, 1, project_group_idx_list, 2)
        xlsx.apply_outer_border(workbook, worksheet, 1, project_group_idx_list, last_col_idx=1)

        last_col_idx = len(self.table_headers) + len(self.year_labels) * 12 - 1
        xlsx.apply_outer_border(workbook, worksheet, self.table_headers.index('Project'), project_group_idx_list, last_col_idx=self.table_headers.index('MP date'))
        xlsx.apply_outer_border(workbook, worksheet, self.table_headers.index('Project'), project_group_idx_list, last_col_idx=last_col_idx)
        xlsx.apply_outer_border(workbook, worksheet, self.table_headers.index('Project'), category_group_idx_list, last_col_idx=last_col_idx)

        for i in range(len(self.year_labels)):
            start_idx = len(self.table_headers) + i * 12
            end_idx = start_idx + 12 - 1
            xlsx.apply_outer_border(workbook, worksheet, start_idx, project_group_idx_list, last_col_idx=end_idx)

        workbook.close()
        self.showMessageBox(u"导出成功!")

    def GetCellType(self, row_idx, col_idx):
        # Cell type
        # 0. EMPTY
        # 1. FILL GREEN, Sync WW
        # 2. FILL YELLOW, No Sync
        # 3. FILL RED, MP
        # 4. FILL GRAY, Phase out
        # [(r, g, b, a)]
        colors = [(255, 255, 255, 255), (0, 255, 0, 255), (255, 255, 0, 255), (255, 0, 0)]
        bkg = self.summary_grid.GetCellBackgroundColour(row_idx, col_idx)
        if bkg in colors:
            return colors.index(bkg)
        return 0

    def OnProjectListRightClick(self, event):
        self.selected_row_index = event.GetText()

        self.sku_menu_itemms = [
            {'id': ControlID.SKU_LIST_MENU_VIEW, 'title': u"查看"},
            {'id': ControlID.SKU_LIST_MENU_DELETE, 'title': u"删除"}
            ]

        menu = self.BuildMenuItems(self.sku_menu_itemms, self.OnMenuSelect_SkuList)
        self.PopupMenu(menu, event.GetPoint())
        menu.Destroy()

    def OnMenuSelect_SkuList(self, event, menu_item, path):
        id = event.GetId()
        if id == ControlID.SKU_LIST_MENU_DELETE:
            self.selected_row_index = int(self.selected_row_index) - 1
            msg = Action().ProjectMgr.del_project(self.project_list[self.selected_row_index].id)
            if msg:
                self.InitData()
            else:
                self.showMessageBox(msg.message)

    def OnActive(self):
        #print '%s has been actived' % self.GetTitle()
        parent_menubar = self.GetParent().GetMenuBar()
        pos = parent_menubar.FindMenu(self.menu_title_in_menubar)
        if pos == -1:
            parent_menubar.Insert(1, self.menu, self.menu_title_in_menubar)

    def OnDeactive(self):
        #print '%s has been deactived' % self.GetTitle()
        parent_menubar = self.GetParent().GetMenuBar()
        pos = parent_menubar.FindMenu(self.menu_title_in_menubar)
        if pos != -1:
            parent_menubar.Remove(pos)

