#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Build-in / Std

'''
Created on 2016年9月1日

@author: Hawk
'''

import wx

class IBaseFunction:
    def __init__(self):
        pass
    def BuildMenuItems(self, menu_items, callback, root=None, path=None):
        '''
        menu_items = [
            {'id': id_1, 'title': u"title", 'separator': True/False, 'IsShown': lambda : True},
            {'id': id_2, 'title': u"title2", 'hasSub': True, 'separator': True/False, 'IsShown': lambda : True
                'sub': [
                    {'id': sub_id_1, 'title': u"sub title 1", 'separator': True, 'IsShown': lambda : True},
                    {'id': sub_id_2, 'title': u"sub title 2",  'separator': True, 'IsShown': lambda : True},
                    ]},
        ]
        '''
        if root is None:
            root = wx.Menu()
            menu = root
        else:
            menu = wx.Menu()
        if path is None:
            path = []

        for item in menu_items:
            if item.has_key('IsShown') and item['IsShown']() == False:
                pass
            else:
                if item.has_key('hasSub') and item['hasSub']:
                    t_path = [x for x in path]
                    t_path.append(item)
                    sub_menu = self.BuildMenuItems(item['sub'], callback, self, t_path)
                    menu.AppendSubMenu(sub_menu, item['title'])
                else:
                    menu.Append(item['id'], item['title'])
                    t_path = [x for x in path]
                    t_path.append(item)
                    wx.EVT_MENU(self, item['id'], lambda event, temp_item=item, temp_path=t_path: callback(event, temp_item, temp_path))
                if item.has_key('separator') and item['separator']:
                    menu.AppendSeparator()

        return menu
