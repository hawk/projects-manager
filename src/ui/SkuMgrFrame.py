#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Build-in / Std

'''
Created on 2016年9月1日

@author: Hawk
'''
import sys
import wx
import MDIChildFrameBase
import BaseDialog
from model.Model import *
from action.Action import *
from ControlID import ControlID

class AddSkuDialog(BaseDialog.BaseDialog):
    def __init__(self, *args, **kwds):
        BaseDialog.BaseDialog.__init__(
            self, None, -1, u'Add SKU',
            style=wx.CAPTION | wx.CLOSE_BOX,
            size=(200, 160))
        self.label_name = wx.StaticText(self, -1, u"名称", pos=(20, 20))
        #self.label_name.SetFont(wx.Font(wx.FONTSIZE_MEDIUM, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL))
        self.textctrl_name = wx.TextCtrl(self, -1, size=(100, 20), pos=(60, 20))
        self.label_comment = wx.StaticText(self, -1, u"备注", pos=(20, 60))
        self.textctrl_comment = wx.TextCtrl(self, -1, size=(100, 20), pos=(60, 60))
        self.button_save = wx.Button(self, -1, u"保存", pos=(50, 90))
        self.Bind(wx.EVT_BUTTON, self.OnSaveButtonClicked, self.button_save)
    def OnSaveButtonClicked(self, event):
        name = self.textctrl_name.GetValue().strip()
        comment = self.textctrl_comment.GetValue().strip()
        msg = Action().SkuMgr.add_sku(name, comment)
        if msg:
            self.EndModal(wx.ID_OK)
        else:
            self.showMessageBox(msg.message)

class SkuMgrFrame(MDIChildFrameBase.MDIChildFrameBase):
    menu_title_in_menubar = "&Sku"

    def __init__(self, *args, **kwargs):
        MDIChildFrameBase.MDIChildFrameBase.__init__(self, *args, **kwargs)
        self.InitMenu()

        self.panel = wx.Panel(self)
        self.ListCtrl_SkuList = wx.ListCtrl(
            self.panel,
            - 1,
            style=wx.LC_REPORT)

        for col, text in enumerate([u'序号', u'id', u'名称', u'创建时间', u'更新时间', u'备注']):
            self.ListCtrl_SkuList.InsertColumn(col, text)

        wx.EVT_LIST_ITEM_RIGHT_CLICK(
            self.ListCtrl_SkuList,
            - 1,
            self.OnSkuListRightClick)

        self.InitData()

        self.sizer_left = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer_left.Add(self.ListCtrl_SkuList, 1, wx.EXPAND | wx.ALL)
        self.panel.SetSizer(self.sizer_left)
        self.sizer_left.Fit(self)

    def InitData(self):
        self.sku_list = Sku.select().where(Sku.deleted == False)
        self.ListCtrl_SkuList.DeleteAllItems()
        for idx, item in enumerate(self.sku_list):
            index = self.ListCtrl_SkuList.InsertStringItem(sys.maxint, str(idx + 1))
            self.ListCtrl_SkuList.SetStringItem(index, 1, str(item.id))
            self.ListCtrl_SkuList.SetStringItem(index, 2, item.name)
            self.ListCtrl_SkuList.SetStringItem(index, 3, str(item.create_time))
            self.ListCtrl_SkuList.SetStringItem(index, 4, str(item.update_time))
            self.ListCtrl_SkuList.SetStringItem(index, 5, item.comment if item.comment is not None else u"")
        self.ListCtrl_SkuList.SetColumnWidth(0, 45)
        self.ListCtrl_SkuList.SetColumnWidth(3, 175)
        self.ListCtrl_SkuList.SetColumnWidth(4, 175)
        self.ListCtrl_SkuList.SetColumnWidth(5, 175)

    def InitMenu(self):
        self.menuBar_menubar_menu_items = [
            {'id': ControlID.MENUBAR_MENU_ITEM_SKU_ADD, 'title': u'Add', 'separator': False, 'IsShown': lambda : True},
            {'id': ControlID.MENUBAR_MENU_ITEM_SKU_REFRESH, 'title': u'Refresh', 'separator': False, 'IsShown': lambda : True,
                'hasSub': False,
                'sub' : [{'id': ControlID.MENUBAR_MENU_ITEM_CATEGORY_ADD, 'title': u"导出为CHM(UTF-8)"}]},
            ]
        self.menu = self.BuildMenuItems(self.menuBar_menubar_menu_items, self.OnMenubarMenu_Select)

    def OnMenubarMenu_Select(self, event, menu_item, path):
        id = event.GetId()
        if id == ControlID.MENUBAR_MENU_ITEM_SKU_ADD:
            dlg = AddSkuDialog()
            if wx.ID_OK == dlg.ShowModal():
                self.InitData()
            dlg.Destroy()
        elif id == ControlID.MENUBAR_MENU_ITEM_SKU_REFRESH:
            self.InitData()

    def OnSkuListRightClick(self, event):
        self.selected_row_index = event.GetText()

        self.sku_menu_itemms = [
            {'id': ControlID.SKU_LIST_MENU_VIEW, 'title': u"查看"},
            {'id': ControlID.SKU_LIST_MENU_DELETE, 'title': u"删除"}
            ]

        menu = self.BuildMenuItems(self.sku_menu_itemms, self.OnMenuSelect_SkuList)
        self.PopupMenu(menu, event.GetPoint())
        menu.Destroy()

    def OnMenuSelect_SkuList(self, event, menu_item, path):
        id = event.GetId()
        if id == ControlID.SKU_LIST_MENU_DELETE:
            self.selected_row_index = int(self.selected_row_index) - 1
            msg = Action().SkuMgr.del_sku(self.sku_list[self.selected_row_index].id)
            if msg:
                self.InitData()
            else:
                self.showMessageBox(msg.message)
        elif id == ControlID.SKU_LIST_MENU_VIEW:
            self.selected_row_index = int(self.selected_row_index) - 1
            print len(self.sku_list[self.selected_row_index].products)
            for p in self.sku_list[self.selected_row_index].products:
                print p.project.name, p.sku.name

    def OnActive(self):
        #print '%s has been actived' % self.GetTitle()
        parent_menubar = self.GetParent().GetMenuBar()
        pos = parent_menubar.FindMenu(self.menu_title_in_menubar)
        if pos == -1:
            parent_menubar.Insert(1, self.menu, self.menu_title_in_menubar)

    def OnDeactive(self):
        #print '%s has been deactived' % self.GetTitle()
        parent_menubar = self.GetParent().GetMenuBar()
        pos = parent_menubar.FindMenu(self.menu_title_in_menubar)
        if pos != -1:
            parent_menubar.Remove(pos)

