#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Build-in / Std

'''
Created on 2016年8月24日

@author: Hawk
'''
import wx
import os
import images_icon
from wx.lib.pubsub import pub as Publisher
from SkuMgrFrame import SkuMgrFrame
from CategoryMgrFrame import CategoryMgrFrame
from ProjectMgrFrame import ProjectMgrFrame
from ProductMgrFrame import ProductMgrFrame
from RecordMgrFrame import RecordMgrFrame
from SummaryByProductFrame import SummaryByProductFrame
from SummaryByProjectFrame import SummaryByProjectFrame
from ControlID import ControlID
from IBaseFunction import IBaseFunction
from action import Action

_file_chose_wildcard_ = "Project database file(*.db)|*.db|All files (*.*)|*.*"
_file_chose_caption_ = "Choose a file"

class MainMDIFrame(wx.MDIParentFrame, IBaseFunction):
    def __init__(self):
        wx.MDIParentFrame.__init__(self, None, -1, u'Projects manager', size=(600, 400))
        self.SetIcon(images_icon.AppIcon.GetIcon())
        self.SetBackgroundColour(wx.Colour(240, 0, 0))
        self.InitMenuBar()

    def InitMenuBar(self):
        self.menuBar_menubar_menu_items = [
            {'id': ControlID.MENUBAR_MENU_ITEM_OPEN_SKU_WINDOW, 'title': u'&Sku', 'separator': False, 'IsShown': lambda : True},
            {'id': ControlID.MENUBAR_MENU_ITEM_OPEN_CATEGORY_WINDOW, 'title': u'&Category', 'separator': False, 'IsShown': lambda : True},
            {'id': ControlID.MENUBAR_MENU_ITEM_OPEN_PROJECT_WINDOW, 'title': u'&Project', 'separator': False, 'IsShown': lambda : True},
            {'id': ControlID.MENUBAR_MENU_ITEM_OPEN_PRODUCT_WINDOW, 'title': u'&Product', 'separator': False, 'IsShown': lambda : True},
            {'id': ControlID.MENUBAR_MENU_ITEM_OPEN_RECORD_WINDOW, 'title': u'&Record', 'separator': False, 'IsShown': lambda : True},
            {'id': ControlID.MENUBAR_MENU_ITEM_OPEN_SUMMARY_PRODUCT_WINDOW, 'title': u'&Summary Product', 'separator': False, 'IsShown': lambda : True},
            {'id': ControlID.MENUBAR_MENU_ITEM_OPEN_SUMMARY_PROJECT_WINDOW, 'title': u'&Summary Project', 'separator': True, 'IsShown': lambda : True},
            {'id': ControlID.MENUBAR_MENU_ITEM_CLOSE_DATABASE, 'title': u'&Close Database', 'separator': False, 'IsShown': lambda : True},
            {'id': ControlID.MENUBAR_MENU_ITEM_CLOSE_APP, 'title': u'E&xit', 'separator': False, 'IsShown': lambda : True},
            ]
        self.menu = self.BuildMenuItems(self.menuBar_menubar_menu_items, self.OnMenubarMenu_Select)

        self.menuBar_menubar_menu_init_items = [
            {'id': ControlID.MENUBAR_MENU_ITEM_CREATE_NEW_DATABASE_WINDOW, 'title': u'&New Database', 'separator': False, 'IsShown': lambda : True},
            {'id': ControlID.MENUBAR_MENU_ITEM_OPEN_DATABASE_WINDOW, 'title': u'&Open Database', 'separator': True, 'IsShown': lambda : True},
            {'id': ControlID.MENUBAR_MENU_ITEM_CLOSE_APP, 'title': u'E&xit', 'separator': False, 'IsShown': lambda : True},
            ]
        self.menu_before_open_db = self.BuildMenuItems(self.menuBar_menubar_menu_init_items, self.OnMenubarMenu_Select)

        self.menubar = wx.MenuBar()
        self.menubar.Append(self.menu_before_open_db, "&File")
        self.SetMenuBar(self.menubar)

    def OnMenubarMenu_Select(self, event, menu_item, path):
        id = event.GetId()
        if id == ControlID.MENUBAR_MENU_ITEM_OPEN_SKU_WINDOW:
            self.OnNewSku(event)
        elif id == ControlID.MENUBAR_MENU_ITEM_OPEN_CATEGORY_WINDOW:
            self.OnNewCategory(event)
        elif id == ControlID.MENUBAR_MENU_ITEM_OPEN_PROJECT_WINDOW:
            self.OnNewProject(event)
        elif id == ControlID.MENUBAR_MENU_ITEM_OPEN_PRODUCT_WINDOW:
            self.OnNewProduct(event)
        elif id == ControlID.MENUBAR_MENU_ITEM_OPEN_RECORD_WINDOW:
            self.OnNewRecord(event)
        elif id == ControlID.MENUBAR_MENU_ITEM_OPEN_SUMMARY_PRODUCT_WINDOW:
            self.OnNewSummary(event)
        elif id == ControlID.MENUBAR_MENU_ITEM_OPEN_SUMMARY_PROJECT_WINDOW:
            self.OnNewProjectSummary(event)
        elif id == ControlID.MENUBAR_MENU_ITEM_CLOSE_DATABASE:
            # remove menu items
            pos = self.menubar.FindMenu("&File")
            if pos != -1:
                self.menubar.Remove(pos)
                self.menubar.Insert(0, self.menu_before_open_db, "&File")
                children_attr_list = ['sku_mgr_frame', 'category_mgr_frame',
                                      'project_mgr_frame', 'product_mgr_frame',
                                      'record_mgr_frame', 'summary_product_frame',
                                       'summary_project_frame']
                for child_attr in children_attr_list:
                    if hasattr(self, child_attr) and getattr(self, child_attr) is not None:
                        getattr(self, child_attr).Close()
                        getattr(self, child_attr).Destroy()
                        setattr(self, child_attr, None)

        elif id == ControlID.MENUBAR_MENU_ITEM_CLOSE_APP:
            self.Close()
        elif id == ControlID.MENUBAR_MENU_ITEM_CREATE_NEW_DATABASE_WINDOW:
            dialog = wx.FileDialog(None, _file_chose_caption_, os.getcwd(),
                    "", _file_chose_wildcard_, wx.SAVE)
            if dialog.ShowModal() == wx.ID_OK:
                Action.Action().create_new_db(dialog.GetPath())
                if Action.Action().success:
                    self.showMessageBox(u'数据库创建成功!')
                else:
                    self.showMessageBox(u'数据库创建失败!')
        elif id == ControlID.MENUBAR_MENU_ITEM_OPEN_DATABASE_WINDOW:
            dialog = wx.FileDialog(None, _file_chose_caption_, os.getcwd(),
                    "", _file_chose_wildcard_, wx.OPEN)
            if dialog.ShowModal() == wx.ID_OK:
                Action.Action().open_db(dialog.GetPath())
                if Action.Action().success:
                    pos = self.menubar.FindMenu("&File")
                    if pos != -1:
                        self.menubar.Remove(pos)
                        self.menubar.Append(self.menu, "&File")
                else:
                    self.showMessageBox(u'文件被加密或者不是数据库文件!')

    def showMessageBox(self, text, caption=u"提示", style=wx.OK):
        dlg = wx.MessageDialog(None, text, caption, style)
        if dlg.ShowModal() == wx.ID_YES:
            self.Close(True)
        dlg.Destroy()

    def OnNewSku(self, evt):
        if not hasattr(self, 'sku_mgr_frame') or self.sku_mgr_frame is None:
            self.sku_mgr_frame = SkuMgrFrame(self, -1, "SkuMgrFrame")
            self.sku_mgr_frame.Show(True)
        else:
            self.sku_mgr_frame.Activate()

    def OnNewCategory(self, evt):
        if not hasattr(self, 'category_mgr_frame') or self.category_mgr_frame is None:
            self.category_mgr_frame = CategoryMgrFrame(self, -1, "CategoryMgrFrame")
            self.category_mgr_frame.Show(True)
        else:
            self.category_mgr_frame.Activate()

    def OnNewProject(self, evt):
        if not hasattr(self, 'project_mgr_frame') or self.project_mgr_frame is None:
            self.project_mgr_frame = ProjectMgrFrame(self, -1, "ProjectMgrFrame")
            self.project_mgr_frame.Show(True)
        else:
            self.project_mgr_frame.Activate()

    def OnNewProduct(self, evt):
        if not hasattr(self, 'product_mgr_frame') or self.product_mgr_frame is None:
            self.product_mgr_frame = ProductMgrFrame(self, -1, "ProductMgrFrame")
            self.product_mgr_frame.Show(True)
        else:
            self.product_mgr_frame.Activate()
    def OnNewRecord(self, evt):
        if not hasattr(self, 'record_mgr_frame') or self.record_mgr_frame is None:
            self.record_mgr_frame = RecordMgrFrame(self, -1, "RecordMgrFrame")
            self.record_mgr_frame.Show(True)
        else:
            self.record_mgr_frame.Activate()
    def OnNewSummary(self, evt):
        if not hasattr(self, 'summary_product_frame') or self.summary_product_frame is None:
            self.summary_product_frame = SummaryByProductFrame(self, -1, "SummaryProductFrame")
            self.summary_product_frame.Show(True)
        else:
            self.summary_product_frame.Activate()
    def OnNewProjectSummary(self, evt):
        if not hasattr(self, 'summary_project_frame') or self.summary_project_frame is None:
            self.summary_project_frame = SummaryByProjectFrame(self, -1, "SummaryProjectFrame")
            self.summary_project_frame.Show(True)
        else:
            self.summary_project_frame.Activate()
