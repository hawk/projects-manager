#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Build-in / Std

'''
Created on 2016年9月2日

@author: Hawk
'''
import wx

class BaseDialog(wx.Dialog):
    def __init__(self, *args, **kwds):
        wx.Dialog.__init__(self, *args, **kwds)

    def showMessageBox(self, text, caption=u"提示", style=wx.OK):
        dlg = wx.MessageDialog(None, text, caption, style)
        if dlg.ShowModal() == wx.ID_YES:
            self.Close(True)
        dlg.Destroy()

    def getTextCtrlValueDefaultNone(self, ctrl):
        value = ctrl.GetValue().strip()
        return None if cmp(value, '') == 0 else value

    def BindEx(self, ctrl, ctrl_id, event_id, callback, **kwargs):
        ctrl.SetId(ctrl_id)
        self.Bind(
                  event_id,
                  lambda event, id=ctrl_id, a_kwargs=kwargs : callback(event, id, **a_kwargs),
                  ctrl)
    def BindButton(self, ctrl, ctrl_id, callback, **kwargs):
        return self.BindEx(ctrl, ctrl_id, wx.EVT_BUTTON, callback, **kwargs)
    def BindMenu(self, ctrl, ctrl_id, callback, **kwargs):
        return self.BindEx(ctrl, ctrl_id, wx.EVT_MENU, callback, **kwargs)
    def BinCheckBox(self, ctrl, ctrl_id, callback, **kwargs):
        return self.BindEx(ctrl, ctrl_id, wx.EVT_CHECKBOX, callback, **kwargs)
