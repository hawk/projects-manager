#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Build-in / Std

'''
Created on 2016年8月24日

@author: Hawk
'''
import wx
import images_icon
import PanelBase
import SkuMgrPanel
from ConfigPanel import ConfigPanel
from wx.lib.pubsub import pub as Publisher

class MainFrame(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, None, -1, u'Projects manager', size=(900, 900))
        self.SetIcon(images_icon.AppIcon.GetIcon())
        self.SetBackgroundColour(wx.Colour(240, 0, 0))

        self.configpanel = wx.Panel(self)

        self.sizer_left = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer_left.Add(self.configpanel, 2, wx.EXPAND | wx.ALL)

        self.ListCtrl_SkuList1 = wx.ListCtrl(
            self,
            - 1,
            style=wx.LC_REPORT)
        self.sizer_left.Add(self.ListCtrl_SkuList1, 1, wx.EXPAND | wx.ALL)

        self.ListCtrl_SkuList = wx.ListCtrl(
            self.configpanel,
            - 1,
            style=wx.LC_REPORT)
        self.ListCtrl_CategoryList = wx.ListCtrl(
            self.configpanel,
            - 1,
            style=wx.LC_REPORT)
        self.ListCtrl_ProjectList = wx.ListCtrl(
            self.configpanel,
            - 1,
            style=wx.LC_REPORT)
        self.ListCtrl_ProjectSkuList = wx.ListCtrl(
            self.configpanel,
            - 1,
            style=wx.LC_REPORT)

        for col, text in enumerate([u'序号', u'id', u'名称', u'创建时间', u'1']):
            self.ListCtrl_SkuList.InsertColumn(col, text)
            self.ListCtrl_CategoryList.InsertColumn(col, text)
            self.ListCtrl_ProjectList.InsertColumn(col, text)
            self.ListCtrl_ProjectSkuList.InsertColumn(col, text)
            self.ListCtrl_SkuList1.InsertColumn(col, text)

        self.main_sizer = wx.BoxSizer(wx.VERTICAL)
        self.top_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.bottom_sizer = wx.BoxSizer(wx.HORIZONTAL)

        self.main_sizer.Add(self.top_sizer, proportion=1, flag=wx.EXPAND, border=1)
        self.main_sizer.Add(self.bottom_sizer, proportion=1, flag=wx.EXPAND, border=1)

        self.top_sizer.Add(self.ListCtrl_SkuList, proportion=1, flag=wx.EXPAND, border=1)
        self.top_sizer.Add(self.ListCtrl_CategoryList, proportion=1, flag=wx.EXPAND, border=1)

        self.bottom_sizer.Add(self.ListCtrl_ProjectList, proportion=1, flag=wx.EXPAND, border=1)
        self.bottom_sizer.Add(self.ListCtrl_ProjectSkuList, proportion=1, flag=wx.EXPAND, border=1)

        self.configpanel.SetSizer(self.main_sizer)
        self.main_sizer.Fit(self.configpanel)

        self.SetSizer(self.sizer_left)
        self.sizer_left.Fit(self)
        self.Layout()
        self.Center()

        Publisher.subscribe(self.__onObjectAdded, 'click_on_panel')

        #self.Bind(wx.EVT_SIZE, self.OnSize, self)

    def OnSize(self, var):
        print '111111111'
        #self.sizer_left.Fit(self)
        #self.configpanel.Refresh()
        #self.configpanel.sizer.Fit(self.configpanel)

    def __onObjectAdded(self, data, extra1, extra2=None):
        # no longer need to access data through message.data.
        print 'Object', repr(data), 'is added'
        print extra1
        print extra2
