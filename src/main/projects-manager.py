#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Build-in / Std

'''
Created on 2016年8月24日

@author: Hawk
'''
import wx
from action import Action
from ui.MainMDIFrame import MainMDIFrame

if __name__ == '__main__':
    Action.Action()
    app = wx.App()
    frame = MainMDIFrame()
    frame.Show()
    app.MainLoop()
