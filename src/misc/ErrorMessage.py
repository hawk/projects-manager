#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Build-in / Std
from repr import repr

'''
Created on 2016年8月24日

@author: Hawk
'''

class ErrorMessage(object):
    def __init__(self, status, message=None):
        self.status = status
        self.message = message if message else ''

    def __eq__(self, other):
        if type(other) == type(True):
            return self.status == other
        elif type(other) == ErrorMessage:
            return self.status == other.status
        else:
            return self == other

    def __ne__(self, other):
        if type(other) == type(True):
            return self.status != other
        elif type(other) == ErrorMessage:
            return self.status != other.status
        else:
            return self != other

    def __nonzero__(self):
        return self.status

    def __str__(self):
        return "[{}, '{}']".format(self.status, self.message)

    def __and__(self, other):
        return self.status and other

    def __or__(self, other):
        return self.status or other
