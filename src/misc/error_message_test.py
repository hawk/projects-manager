#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Build-in / Std
from repr import repr

'''
Created on 2016年8月24日

@author: Hawk
'''
from misc.ErrorMessage import ErrorMessage
import unittest

# testcase
class ErrorMessageTestcase(unittest.TestCase):
    def setUp(self):
        self.object1 = ErrorMessage(True, 'True value test')
        self.object2 = ErrorMessage(False, 'False value test')
        self.object3 = ErrorMessage(True, 'True value test 2')
    def tearDown(self):
        self.object1 = None
        self.object2 = None
        self.object3 = None
    def testobject(self):
        self.assertTrue(self.object1)
        self.assertFalse(self.object2)
    def eq__test(self):
        self.assertEqual(self.object1, True)
        self.assertEqual(self.object1, self.object3)
    def ne__test(self):
        self.assertNotEqual(self.object1, False)
        self.assertNotEqual(self.object1, self.object2)
    def nonzero__test(self):
        self.assertTrue(self.object1)
        self.assertFalse(self.object2)
    def str_test(self):
        self.assertEqual("[True, 'True value test']", str(self.object1))
        self.assertEqual("[False, 'False value test']", str(self.object2))
    def and_test(self):
        self.assertTrue(self.object1 and True)
        self.assertFalse(self.object2 and True)
    def or_test(self):
        self.assertTrue(self.object1 or False)
        self.assertFalse(self.object2 and False)
    def not_test(self):
        self.assertTrue(not self.object2)

if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(ErrorMessageTestcase("testobject"))
    suite.addTest(ErrorMessageTestcase("eq__test"))
    suite.addTest(ErrorMessageTestcase("ne__test"))
    suite.addTest(ErrorMessageTestcase("nonzero__test"))
    suite.addTest(ErrorMessageTestcase("str_test"))
    suite.addTest(ErrorMessageTestcase("and_test"))
    suite.addTest(ErrorMessageTestcase("or_test"))
    suite.addTest(ErrorMessageTestcase("not_test"))

    runner = unittest.TextTestRunner()
    runner.run(suite)
