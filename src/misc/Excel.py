#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Build-in / Std

'''
Created on 2016年9月13日

@author: Hawk
'''

import sys
import codecs
import xdrlib
import xlrd
import xlwt
import re
import HTMLParser
import xml.dom.minidom
from datetime import datetime

reload(sys)
sys.setdefaultencoding('utf8')
codecs.register(
    lambda name: codecs.lookup('utf-8') if name == 'cp65001' else None)

xlwt.add_palette_colour("sync_ww_bkg_color_green", 0x21)

INDENT = "    "
htmlParser = HTMLParser.HTMLParser()
cell_style = xlwt.easyxf('font: height 240,name Calibri; align: wrap on, vert centre, horiz centre')
cell_style_green_bkg = xlwt.easyxf('font: height 240,name Calibri; align: wrap on, vert centre, horiz centre; pattern: pattern solid, fore_colour sync_ww_bkg_color_green')
cell_style_yellow_bkg = xlwt.easyxf('font: height 240,name Calibri; align: wrap on, vert centre, horiz centre; pattern: pattern solid, fore_colour yellow')
cell_style_align_left = xlwt.easyxf('font: height 240,name Calibri; align: wrap on, vert centre, horiz left')
hyperlink_style = xlwt.easyxf('font: name Times New Roman, underline single,color 4;align: wrap on, vert centre, horiz left')
header_cell_style = xlwt.easyxf('font: height 320,name Times New Roman, color-index red, bold on; align:vert centre, horiz center')
datetime_style = xlwt.easyxf('font: height 240,name Calibri; align: wrap on, vert centre, horiz left')
datetime_style.num_format_str = 'dd/mm/yyyy'

def write_header(table, row_idx, col_idx, content):
    table.write(row_idx, col_idx, content, header_cell_style)

def write_hyperlink(table, row_idx, col_idx, content, title=None):
    if title is None:
        title = content
    table.write(row_idx, col_idx, xlwt.Formula('HYPERLINK("%s";"%s")' % (content, title)), hyperlink_style)

def write_cell1(table, row_idx, col_idx, content, style=cell_style, row_height=1, col_width=1):
    target_row = row_idx + row_height - 1
    target_col = col_idx + col_width - 1
    table.write_merge(row_idx, target_row, col_idx, target_col, content, style)

def write_cell_align_left(table, row_idx, col_idx, content, style=cell_style_align_left, row_height=1, col_width=1):
    return write_cell1(table, row_idx, col_idx, content, style, row_height, col_width)
