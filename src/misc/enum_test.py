#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Build-in / Std
from repr import repr

'''
Created on 2016年8月24日

@author: Hawk
'''
from misc.Enum import Enum
from misc.ErrorMessage import ErrorMessage
import unittest

# testcase
class EnumTestcase(unittest.TestCase):
    def setUp(self):
        self.wID = Enum([
                "DOG=11",
                "CAT = DOG + 2 * (3 % 2)",
                "HORSE",
                "DESK",
                "FISH",
            ])
    def tearDown(self):
        self.wID = None
    def testvalue(self):
        self.assertEqual(self.wID.DOG, 11)
        self.assertEqual(self.wID.CAT, 13)
        self.assertEqual(self.wID.HORSE, 14)
        self.assertEqual(self.wID.DESK, 15)
        self.assertEqual(self.wID.FISH, 16)
        self.assertEqual(self.wID.FISH.name, 'FISH')
    def duplicate_test(self):
        try:
            # Duplicated names
            wDuplicateID = Enum([
                "DOG",
                "CAT",
                "DOG"
            ]) # Duplicated enum names
        except AttributeError as e:
            self.assertEqual(e.message, 'Duplicated enum names: DOG')

if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(EnumTestcase("testvalue"))
    suite.addTest(EnumTestcase("duplicate_test"))

    runner = unittest.TextTestRunner()
    runner.run(suite)
