#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Build-in / Std

'''
Created on 2016年9月13日

@author: Hawk
'''

from Model import *
import os
import datetime

def category_setup():
    # add Category data
    categories = [
                  {'name': 'Recreational'},
                  {'name': 'Motorcycle'},
                  {'name': 'Golf'},
                  {'name': 'Watch'},
                  {'name': 'Camera'},
                  ]
    for c in categories:
        cat = Category()
        cat.name = c['name']
        cat.save()

def project_setup():
    projects = [
                {'name': 'eTrex 10', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'eTrex C', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'eTrex 20/30', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'eTrex 201/301', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'eTrex 209/309', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'eTrex 20x/30x', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'eTrex 201x/301x', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'eTrex 209x/309x', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},

                {'name': 'GPSMAP 62SC', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'GPSMAP 621', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'GPSMAP 629', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'GPSMAP 62/78', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},

                {'name': 'Oregon 450', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'Oregon 550', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'Oregon 650', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'Oregon 650(TCJ2)', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'Rino 650', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'Montana 650', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},

                {'name': 'GPSMAP64', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'eTrex Touch35', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'Oregon7xx', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'GPSMAP64SC', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},

                {'name': 'Zumo 660', 'category': 2, 'owner': 'Best', 'ee_owner': 'Kevin'},

                {'name': 'Approach S2', 'category': 3, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'Approach S3', 'category': 3, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'Approach S4', 'category': 3, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'Approach S6', 'category': 3, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'Approach S20', 'category': 3, 'owner': 'Best', 'ee_owner': 'Kevin'},

                {'name': 'Fenix', 'category': 4, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'Quatix', 'category': 4, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'Fenix 2', 'category': 4, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'Fenix 3', 'category': 4, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'Fenix3 HR', 'category': 4, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'Epix', 'category': 4, 'owner': 'Best', 'ee_owner': 'Kevin'},
                {'name': 'D2 Bravo', 'category': 4, 'owner': 'Best', 'ee_owner': 'Kevin'},

                {'name': 'Virb XE', 'category': 5, 'owner': 'Best', 'ee_owner': 'Kevin'},

                {'name': 'Monterra', 'category': 1, 'owner': 'Best', 'ee_owner': 'Kevin'},
                ]
    for p in projects:
        pro = Project()
        pro.name = p['name']
        pro.category = p['category']
        pro.owner = p['owner']
        pro.ee_owner = p['ee_owner']
        pro.save()

def sku_setup():
    skus = [
            {'name': 'APAC'},
            {'name': 'CHN'},
            {'name': 'CHN GIS'},
            {'name': 'CHN Beidou'},
            {'name': 'TWN'},
            {'name': 'JPN'},
            {'name': 'KOR'},
            {'name': 'SEA'},
            {'name': 'IND'},
            ]
    for s in skus:
        sk = Sku()
        sk.name = s['name']
        sk.save()

def project_sku_setup():
    project_skus = [
            {'project': 'eTrex 10', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2012, 8, 22), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'eTrex 10', 'sku': 'JPN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2011, 11, 29), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'eTrex 10', 'sku': 'SEA', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2012, 3, 3), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'eTrex C', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2015, 2, 2), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'eTrex 20/30', 'sku': 'CHN GIS', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2012, 4, 27), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'eTrex 20/30', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2012, 5, 18), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'eTrex 20/30', 'sku': 'JPN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2012, 1, 4), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'eTrex 20/30', 'sku': 'TWN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2012, 9, 11), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'eTrex 20/30', 'sku': 'SEA', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2012, 3, 1), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'eTrex 201/301', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2013, 3, 13), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'eTrex 209/309', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2013, 8, 26), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'eTrex 20x/30x', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2015, 4, 19), 'mp_date': datetime.date(2015, 07, 29), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'eTrex 20x/30x', 'sku': 'TWN', 'owner': 'Best', 'frm107_date': datetime.date(2015, 5, 25), 'mp_date': datetime.date(2015, 07, 29), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'eTrex 20x/30x', 'sku': 'JPN', 'owner': 'Best', 'frm107_date': datetime.date(2015, 5, 25), 'mp_date': datetime.date(2015, 9, 21), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'eTrex 20x/30x', 'sku': 'SEA', 'owner': 'Best', 'frm107_date': datetime.date(2015, 5, 25), 'mp_date': datetime.date(2015, 8, 24), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'eTrex 201x/301x', 'sku': 'CHN GIS', 'owner': 'Best', 'frm107_date': datetime.date(2015, 04, 19), 'mp_date': datetime.date(2015, 8, 3), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'eTrex 209x/309x', 'sku': 'CHN Beidou', 'owner': 'Best', 'frm107_date': datetime.date(2015, 4, 19), 'mp_date': datetime.date(2015, 9, 17), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'GPSMAP 62SC', 'sku': 'CHN GIS', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2012, 5, 15), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'GPSMAP 62SC', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2012, 5, 15), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'GPSMAP 62SC', 'sku': 'TWN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2012, 9, 19), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'GPSMAP 62SC', 'sku': 'JPN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2012, 6, 19), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'GPSMAP 62SC', 'sku': 'SEA', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2013, 3, 13), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'GPSMAP 621', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2013, 4, 15), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'GPSMAP 629', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 18), 'mp_date': datetime.date(2013, 6, 3), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'GPSMAP 62/78', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2010, 11, 9), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'GPSMAP 62/78', 'sku': 'KOR', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2011, 4, 4), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'GPSMAP 62/78', 'sku': 'SEA', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2011, 3, 29), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'GPSMAP 62/78', 'sku': 'JPN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2011, 10, 11), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'Oregon 450', 'sku': 'JPN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2010, 7, 20), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'Oregon 550', 'sku': 'CHN GIS', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2010, 07, 01), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Oregon 550', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2012, 4, 3), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Oregon 550', 'sku': 'JPN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2012, 4, 9), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Oregon 550', 'sku': 'KOR', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2010, 4, 13), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Oregon 550', 'sku': 'TWN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2009, 8, 19), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Oregon 550', 'sku': 'SEA', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2009, 10, 26), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'Oregon 650', 'sku': 'SEA', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2013, 9, 6), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Oregon 650', 'sku': 'JPN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2013, 11, 18), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Oregon 650(TCJ2)', 'sku': 'JPN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2015, 2, 2), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'Rino 650', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2013, 1, 21), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Rino 650', 'sku': 'KOR', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2014, 5, 20), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'Montana 650', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2013, 1, 21), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Montana 650', 'sku': 'TWN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2012, 8, 27), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Montana 650', 'sku': 'KOR', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2013, 6, 20), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'Virb XE', 'sku': 'APAC', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2015, 8, 4), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'GPSMAP64', 'sku': 'SEA', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2014, 11, 6), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'GPSMAP64', 'sku': 'JPN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2015, 1, 6), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'GPSMAP64', 'sku': 'KOR', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2015, 7, 6), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'GPSMAP64', 'sku': 'TWN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2015, 11, 2), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'eTrex Touch35', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2016, 7, 6), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'eTrex Touch35', 'sku': 'JPN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2016, 8, 15), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'eTrex Touch35', 'sku': 'CHN GIS', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2017, 07, 01), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'Oregon7xx', 'sku': 'APAC', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2016, 07, 01), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Oregon7xx', 'sku': 'CHN Beidou', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2017, 3, 01), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'GPSMAP64SC', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2016, 07, 01), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'GPSMAP64SC', 'sku': 'CHN GIS', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2016, 07, 01), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'GPSMAP64SC', 'sku': 'TWN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2016, 07, 01), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'GPSMAP64SC', 'sku': 'SEA', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2016, 07, 01), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'GPSMAP64SC', 'sku': 'JPN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2016, 07, 01), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'Zumo 660', 'sku': 'TWN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2012, 07, 20), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Zumo 660', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2012, 10, 29), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Zumo 660', 'sku': 'JPN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2010, 2, 01), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'Approach S3', 'sku': 'APAC', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2012, 9, 20), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Approach S2', 'sku': 'APAC', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2014, 4, 7), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Approach S2', 'sku': 'JPN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2014, 4, 17), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Approach S4', 'sku': 'APAC', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2014, 5, 15), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Approach S6', 'sku': 'APAC', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2015, 4, 1), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Approach S20', 'sku': 'APAC', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2016, 07, 14), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'Fenix', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2013, 3, 4), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Fenix', 'sku': 'JPN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2013, 5, 15), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Fenix', 'sku': 'TWN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2013, 5, 10), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Fenix', 'sku': 'KOR', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2013, 11, 20), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'Quatix', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2013, 3, 25), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'Fenix 2', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2014, 07, 01), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Fenix 2', 'sku': 'JPN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2014, 10, 01), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'Fenix 3', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2015, 3, 28), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Fenix 3', 'sku': 'TWN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2015, 4, 1), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Fenix 3', 'sku': 'JPN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2015, 8, 24), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Fenix 3', 'sku': 'SEA', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2015, 11, 16), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Fenix 3', 'sku': 'KOR', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2015, 11, 16), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'Fenix3 HR', 'sku': 'APAC', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2016, 4, 25), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'Epix', 'sku': 'JPN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2016, 2, 28), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'D2 Bravo', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2016, 3, 3), 'development_mm': 0, 'maintenance_mm': 0},

            {'project': 'Monterra', 'sku': 'CHN', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2014, 4, 7), 'development_mm': 0, 'maintenance_mm': 0},
            {'project': 'Monterra', 'sku': 'CHN GIS', 'owner': 'Best', 'frm107_date': datetime.date(2016, 06, 01), 'mp_date': datetime.date(2014, 5, 14), 'development_mm': 0, 'maintenance_mm': 0},
            ]

    def get_project_id_by_name(name):
        return Project.select().where(Project.name == name).get().id
    def get_sku_id_by_name(name):
        return Sku.select().where(Sku.name == name).get().id
    for s in project_skus:
        sk = Product()
        sk.project = get_project_id_by_name(s['project'])
        sk.sku = get_sku_id_by_name(s['sku'])
        sk.owner = s['owner']
        sk.frm107_date = s['frm107_date']
        sk.mp_date = s['mp_date']
        sk.save()

def record_setup():
    records = [
        {'project': 'eTrex 10', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'eTrex 10', 'sku': 'JPN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'eTrex 10', 'sku': 'SEA', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'eTrex C', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'eTrex 20/30', 'sku': 'CHN GIS', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'eTrex 20/30', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'eTrex 20/30', 'sku': 'JPN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'eTrex 20/30', 'sku': 'TWN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'eTrex 20/30', 'sku': 'SEA', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'eTrex 201/301', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'eTrex 209/309', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'eTrex 20x/30x', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'eTrex 20x/30x', 'sku': 'TWN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'eTrex 20x/30x', 'sku': 'JPN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'eTrex 20x/30x', 'sku': 'SEA', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'eTrex 209x/309x', 'sku': 'CHN Beidou', 'release_date': datetime.date(2016, 11, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'GPSMAP 62SC', 'sku': 'CHN GIS', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'GPSMAP 62SC', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'GPSMAP 62SC', 'sku': 'TWN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'GPSMAP 62SC', 'sku': 'JPN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'GPSMAP 62SC', 'sku': 'SEA', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'GPSMAP 621', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'GPSMAP 629', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'GPSMAP 62/78', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'GPSMAP 62/78', 'sku': 'KOR', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'GPSMAP 62/78', 'sku': 'SEA', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'GPSMAP 62/78', 'sku': 'JPN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'Oregon 450', 'sku': 'JPN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'Oregon 550', 'sku': 'CHN GIS', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Oregon 550', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Oregon 550', 'sku': 'JPN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Oregon 550', 'sku': 'KOR', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Oregon 550', 'sku': 'TWN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Oregon 550', 'sku': 'SEA', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'Oregon 650', 'sku': 'SEA', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Oregon 650', 'sku': 'JPN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Oregon 650(TCJ2)', 'sku': 'JPN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'Rino 650', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Rino 650', 'sku': 'KOR', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'Montana 650', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Montana 650', 'sku': 'TWN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Montana 650', 'sku': 'KOR', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'Virb XE', 'sku': 'APAC', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'GPSMAP64', 'sku': 'SEA', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'GPSMAP64', 'sku': 'JPN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'GPSMAP64', 'sku': 'KOR', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'GPSMAP64', 'sku': 'TWN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'eTrex Touch35', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'eTrex Touch35', 'sku': 'JPN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'eTrex Touch35', 'sku': 'CHN GIS', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'Oregon7xx', 'sku': 'APAC', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Oregon7xx', 'sku': 'CHN Beidou', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'GPSMAP64SC', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'GPSMAP64SC', 'sku': 'CHN GIS', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'GPSMAP64SC', 'sku': 'TWN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'GPSMAP64SC', 'sku': 'SEA', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'GPSMAP64SC', 'sku': 'JPN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'Zumo 660', 'sku': 'TWN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Zumo 660', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Zumo 660', 'sku': 'JPN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'Approach S3', 'sku': 'APAC', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Approach S2', 'sku': 'APAC', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Approach S2', 'sku': 'JPN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Approach S4', 'sku': 'APAC', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Approach S6', 'sku': 'APAC', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Approach S20', 'sku': 'APAC', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'Fenix', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Fenix', 'sku': 'JPN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Fenix', 'sku': 'TWN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Fenix', 'sku': 'KOR', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'Quatix', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'Fenix 2', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Fenix 2', 'sku': 'JPN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'Fenix 3', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Fenix 3', 'sku': 'TWN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Fenix 3', 'sku': 'JPN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Fenix 3', 'sku': 'SEA', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'Fenix 3', 'sku': 'KOR', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'Fenix3 HR', 'sku': 'APAC', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},

        {'project': 'Epix', 'sku': 'JPN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        {'project': 'D2 Bravo', 'sku': 'CHN', 'release_date': datetime.date(2016, 1, 1), 'version': '1.00', 'ww_version': '1.00', 'development_mm': 0, 'maintenance_mm': 0, 'is_sync_ww': False},
        ]

    def get_project_id_by_name(name):
        return Project.select().where(Project.name == name).get().id
    def get_sku_id_by_name(name):
        return Sku.select().where(Sku.name == name).get().id
    def get_product_id_by_name(project_name, sku_name):
        project_id = get_project_id_by_name(project_name)
        sku_id = get_sku_id_by_name(sku_name)
        return Product.select().where((Product.project == project_id) & (Product.sku == sku_id)).get().id

    for r in records:
        rec = Record()
        rec.date = r['release_date'];
        rec.product = get_product_id_by_name(r['project'], r['sku']);
        rec.version = r['version'];
        rec.ww_version = r['ww_version'];
        rec.development_mm = r['development_mm'];
        rec.maintenance_mm = r['maintenance_mm'];
        rec.is_sync_ww = r['is_sync_ww'];
        rec.save()

if __name__ == '__main__':
    db_name = 'model_data.db'
    database = SqliteDatabaseWarpper(db_name)

    database.register(Category)
    database.register(Project)
    database.register(Sku)
    database.register(Record)
    database.register(Product)
    database.register(Operation)

    database.connect()
    database.create_tables([Category, Project, Sku, Record, Product, Operation], safe=True)

    category_setup()
    project_setup()
    sku_setup()
    project_sku_setup()
    #record_setup()

    print 'data import done!'
