CREATE TABLE `pm_categories` (
  `id` INTEGER NOT NULL PRIMARY KEY, 
  `name` VARCHAR(255) NOT NULL, 
  `create_time` DATETIME NOT NULL, 
  `update_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
  `comment` VARCHAR(255) DEFAULT NULL
); CREATE TABLE `pm_projects` (
  `id` INTEGER NOT NULL PRIMARY KEY, 
  `name` VARCHAR(255) NOT NULL, 
  `category_id` INTEGER NOT NULL, 
  `owner` VARCHAR(255) NOT NULL, 
  `ee_owner` VARCHAR(255) NOT NULL, 
  `comment` VARCHAR(255) DEFAULT NULL, 
  FOREIGN KEY (`category_id`) REFERENCES `pm_categories` (`id`)
); CREATE TABLE `pm_project_skus` (
  `id` INTEGER NOT NULL PRIMARY KEY, 
  `project_id` INTEGER NOT NULL, 
  `sku_id` INTEGER NOT NULL, 
  `owner` VARCHAR(255) NOT NULL, 
  `frm107_date` INTEGER NOT NULL, 
  `mp_date` INTEGER NOT NULL, 
  `development_mm` REAL NOT NULL, 
  `maintenance_mm` REAL NOT NULL, 
  `comment` VARCHAR(255) DEFAULT NULL, 
  FOREIGN KEY (`project_id`) REFERENCES `pm_projects` (`id`), 
  FOREIGN KEY (`sku_id`) REFERENCES `pm_skus` (`id`)
); CREATE TABLE `pm_records` (
  `id` INTEGER NOT NULL PRIMARY KEY, 
  `date` INTEGER NOT NULL, 
  `project_id` INTEGER NOT NULL, 
  `sku_id` INTEGER NOT NULL, 
  `version` VARCHAR(255) NOT NULL, 
  `ww_version` VARCHAR(255) NOT NULL, 
  `development_mm` REAL NOT NULL, 
  `maintenance_mm` REAL NOT NULL, 
  `is_sync_ww` SMALLINT NOT NULL, 
  `comment` VARCHAR(255) DEFAULT NULL, 
  FOREIGN KEY (`project_id`) REFERENCES `pm_projects` (`id`), 
  FOREIGN KEY (`sku_id`) REFERENCES `pm_skus` (`id`)
); CREATE TABLE `pm_skus` (
  `id` INTEGER NOT NULL PRIMARY KEY, 
  `name` VARCHAR(255) NOT NULL, 
  `create_time` DATETIME NOT NULL, 
  `update_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP, 
  `comment` VARCHAR(255) DEFAULT NULL
);
CREATE UNIQUE INDEX `pm_categories_name` ON `pm_categories` (`name`);
CREATE INDEX `pm_projects_category_id` ON `pm_projects` (`category_id`);
CREATE UNIQUE INDEX `pm_projects_name` ON `pm_projects` (`name`);
CREATE INDEX `pm_project_skus_project_id` ON `pm_project_skus` (`project_id`);
CREATE INDEX `pm_project_skus_sku_id` ON `pm_project_skus` (`sku_id`);
CREATE INDEX `pm_records_project_id` ON `pm_records` (`project_id`);
CREATE INDEX `pm_records_sku_id` ON `pm_records` (`sku_id`);